import React, { Component } from "react";
import propTypes from "prop-types";

export default class FPaises extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    this.state = this.props.elemento;
    this.enviar = this.enviar.bind(this);
  }
  enviar(e) {
    e.preventDefault();
    console.log(this.state);
  }

  render() {
    let {
      country_id,
      iso2,
      short_name,
      spanish_name,
      calling_code,
      cctld,
      iso3,
      long_name,
      numcode,
      un_member,
    } = this.state;

    return (
      <div className="container">
        <form>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="country_id">Id</label>
              <input
                className="form-control"
                id="country_id"
                name="country_id"
                placeholder="Id"
                value={country_id}
                readOnly
              />
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="iso2">ISO2</label>
              <input
                className="form-control"
                id="iso2"
                name="iso2"
                placeholder="Introduce el iso2"
                value={iso2}
                onChange={(e) => this.setState({ iso2: e.target.value })}
              />
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="spanish_name">Spanish Name</label>
              <input
                className="form-control"
                id="spanish_name"
                name="spanish_name"
                placeholder="Introduce el nombre en español"
                value={spanish_name}
                onChange={(e) =>
                  this.setState({ spanish_name: e.target.value })
                }
              />
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="short_name">Short Name</label>
              <input
                className="form-control"
                id="short_name"
                name="short_name"
                placeholder="Introduce el nombre"
                value={short_name}
                onChange={(e) => this.setState({ short_name: e.target.value })}
              />
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="calling_code">Calling code</label>
              <input
                className="form-control"
                id="calling_code"
                name="calling_code"
                placeholder="Introduce el calling code"
                value={calling_code || 0}
                onChange={(e) =>
                  this.setState({ calling_code: e.target.value })
                }
              />
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="cctld">CCTLD</label>
              <input
                className="form-control"
                id="cctld"
                name="cctld"
                placeholder="Introduce el cctld"
                value={cctld || 0}
                onChange={(e) => this.setState({ cctld: e.target.value })}
              />
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="iso3">ISO3</label>
              <input
                className="form-control"
                id="iso3"
                name="iso3"
                placeholder="Introduce el iso3"
                value={iso3 || iso2}
                onChange={(e) => this.setState({ iso3: e.target.value })}
              />
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="long_name">Long name</label>
              <input
                className="form-control"
                id="long_name"
                name="long_name"
                placeholder="Introduce el long name"
                value={long_name || 0}
                onChange={(e) => this.setState({ long_name: e.target.value })}
              />
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="numcode">Num Code</label>
              <input
                className="form-control"
                id="numcode"
                name="numcode"
                placeholder="Introduce el numcode"
                value={numcode}
                onChange={(e) => this.setState({ numcode: e.target.value })}
              />
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="un_member">Un Member</label>
              <input
                className="form-control"
                id="un_member"
                name="un_member"
                placeholder="Introduce el un member "
                value={un_member}
                onChange={(e) => this.setState({ un_member: e.target.value })}
              />
            </div>
          </div>
          <button
            type="submit"
            className="btn btn-primary"
            onClick={this.enviar}
          >
            Editar
          </button>
        </form>
      </div>
    );
  }
}
FPaises.propTypes = {
  elemento: propTypes.shape({
    country_id: propTypes.number,
    iso2: propTypes.string.isRequired,
    short_name: propTypes.string.isRequired,
    spanish_name: propTypes.string.isRequired,
    calling_code: propTypes.string,
    cctld: propTypes.string,
    iso3: propTypes.string,
    long_name: propTypes.string,
    numcode: propTypes.number.isRequired,
    un_member: propTypes.number,
  }),
};
