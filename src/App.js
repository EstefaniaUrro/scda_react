import React from "react";
import "./App.css";
import FPaises from "./Componentes/FPaises";

let pais = {
  country_id: 1,
  iso2: "WK",
  short_name: "Wakanda",
  spanish_name: "Wakanda",
  calling_code: null,
  cctld: null,
  iso3: null,
  long_name: null,
  numcode: 0,
  un_member: 0,
};
function App() {
  return (
    <div className="App">
      <FPaises elemento={pais} />
    </div>
  );
}

export default App;
